package ar.fiuba.tdd.tp1.reader;

/**
 * Reader object that obtains the user's requests from the console.
 */

import java.util.Scanner;

public class ConsoleReader implements Reader {

    Scanner scanner;

    public ConsoleReader() {
        this.scanner = new Scanner(System.in, "UTF-8");
    }

    @Override
    public String read() {
        return this.scanner.nextLine();
    }
}
