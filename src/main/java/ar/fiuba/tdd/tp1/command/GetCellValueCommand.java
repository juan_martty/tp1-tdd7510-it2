package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;


/**
 * Class that simulates the operation of getting the content of a cell in a spreadsheet.
 * It could fail if there is no current spreadsheet open or if the position of the element of interest
 * is invalid. An invalid cell's position means that the spreadsheet doesn't have the required sheet.
 */
public class GetCellValueCommand implements Command {

    CellLocation cellLocation;

    public GetCellValueCommand(CellLocation cellLocation) {
        this.cellLocation = cellLocation;
    }

    @Override
    public void execute(Application application) throws InvalidSheet, NoSpreadsheetOpen, CellWithoutContent,
            ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        if (!application.isASpreadSheetOpen()) {
            throw new NoSpreadsheetOpen();
        }
        application.showCellValue(this.cellLocation);
    }

    @Override
    public Changeable getChangeable() {
        return null;
    }
}
