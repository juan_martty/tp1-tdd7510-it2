package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;

import java.io.IOException;

/**
 * Created by milena on 28/10/15.
 */
public class SaveSpreadSheet implements Command {



    private final String fileName;

    public SaveSpreadSheet(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void execute(Application application) throws NoSpreadsheetOpen, IOException {
        application.writeJson(this.fileName);


    }

    @Override
    public Changeable getChangeable() {
        return null;
    }
}
