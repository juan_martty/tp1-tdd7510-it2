package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.WrongSpreadsheetOrSheetUse;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;

/**
 * Class that initiates the action of undoing last command executed.
 * It might fail if there is no command to be undone.
 */
public class UndoCommand implements Command {

    @Override
    public void execute(Application application) throws WrongSpreadsheetOrSheetUse, CellWithoutContent,
            InvalidFormatException {
        application.undo();

    }

    @Override
    public Changeable getChangeable() {
        return null;
    }
}
