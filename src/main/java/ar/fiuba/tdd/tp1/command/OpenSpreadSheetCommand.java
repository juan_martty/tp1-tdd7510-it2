package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.SpreadSheetIsAlreadyOpen;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.SpreadSheetWasntCreated;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.WrongSpreadsheetOrSheetUse;

/**
 * Class that simulates the action of opening a spreadsheet.
 * It can fail if the spreadsheet that tries to be open hasn't been created previously
 * by the client itself, or if there is another spreadsheet open at the moment.
 */
public class OpenSpreadSheetCommand implements Command, Changeable {

    String nameSpreadSheet;

    public OpenSpreadSheetCommand(String nameSpreadSheet) {
        this.nameSpreadSheet = nameSpreadSheet;
    }

    @Override
    public void undo(Application application) throws WrongSpreadsheetOrSheetUse {
        CloseSpreadSheetCommand reverseCommand = new CloseSpreadSheetCommand();
        reverseCommand.execute(application);
    }

    @Override
    public void redo(Application application) throws SpreadSheetIsAlreadyOpen, SpreadSheetWasntCreated {
        this.execute(application);
    }

    @Override
    public void execute(Application application) throws SpreadSheetIsAlreadyOpen, SpreadSheetWasntCreated {

        if (application.isASpreadSheetOpen()) {
            throw new SpreadSheetIsAlreadyOpen();
        }
        if (!application.openSpreadSheet(this.nameSpreadSheet)) {
            throw new SpreadSheetWasntCreated();
        }
    }

    @Override
    public Changeable getChangeable() {
        return this;
    }


}
