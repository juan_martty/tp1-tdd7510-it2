package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.application.Spreadsheet;
import ar.fiuba.tdd.tp1.cell.Cell;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.InvalidType;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotWhatExpected;

public class SetCellTypeCommand implements Command, Changeable {

    CellContent previousType;
    String type;
    CellLocation location;

    public SetCellTypeCommand(CellLocation loc, String type) {
        this.type = type;
        this.location = loc;
    }

    @Override
    public void execute(Application application) throws InvalidSheet, NoSpreadsheetOpen, ContentNotWhatExpected,
            InvalidType, InvalidCellLocationFormat {
        Cell cell = application.getCell(this.location);
        this.previousType = application.setCellType(this.location, this.type);
        cell.completeContent(application);
    }

    @Override
    public void undo(Application application) throws InvalidSheet {
        Spreadsheet workingSS = application.getWorkingSpreadSheet();
        workingSS.write(this.location, this.previousType);
    }

    @Override
    public void redo(Application application) throws InvalidSheet, NoSpreadsheetOpen, ContentNotWhatExpected,
            InvalidType, InvalidCellLocationFormat {
        this.execute(application);
    }

    @Override
    public Changeable getChangeable() {
        return this;
    }

}
