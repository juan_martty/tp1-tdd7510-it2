package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.exceptions.ProblemsWritingFile;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;

import java.io.IOException;

/**
 * Created by milena on 28/10/15.
 */
public class ShowRanges implements Command {

    @Override
    public void execute(Application application) throws  IOException, NoSpreadsheetOpen {
        application.showRanges();
    }

    @Override
    public Changeable getChangeable() {
        return null;
    }
}
