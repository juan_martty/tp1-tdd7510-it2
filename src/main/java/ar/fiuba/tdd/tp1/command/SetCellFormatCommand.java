package ar.fiuba.tdd.tp1.command;


import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;

import java.util.List;

public class SetCellFormatCommand implements Command, Changeable {

    String oldType;
    String oldFormat;
    String type;
    String format;
    CellLocation location;

    public SetCellFormatCommand(CellLocation loc, String formatType, String format) {
        this.type = formatType;
        this.format = format;
        this.location = loc;
    }

    @Override
    public void execute(Application application) throws InvalidSheet, NoSpreadsheetOpen, InvalidFormatException {
        List<String> oldFormatter = application.setCellFormat(this.location, this.type, this.format);
        this.oldType = oldFormatter.get(0);
        this.oldFormat = oldFormatter.get(1);
    }

    @Override
    public void undo(Application application) throws InvalidSheet, NoSpreadsheetOpen, InvalidFormatException {
        application.setCellFormat(this.location, this.oldType, this.oldFormat);
    }

    @Override
    public void redo(Application application) throws InvalidSheet, NoSpreadsheetOpen, InvalidFormatException {
        this.execute(application);
    }

    @Override
    public Changeable getChangeable() {
        return this;
    }


}
