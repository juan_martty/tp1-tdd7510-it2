package ar.fiuba.tdd.tp1.application;

import ar.fiuba.tdd.tp1.cell.formatters.FormatInformation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by milena on 28/10/15.
 */

public class CellInformation {
    private String sheet;
    private String id;
    private String content;
    private String type;
    private List<FormatInformation> formatters;
    //    private FormatInformation formatter;
    private transient String cellValue;

    CellInformation() {
        this.formatters = new ArrayList<>();
//        this.formatter = new FormatInformation();
    }

    public void setSheet(String sheet) {
        this.sheet = sheet;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCellFormat(ArrayList<FormatInformation> formatters) {
        if (formatters != null) {
            this.formatters = formatters;
        }
    }

    public void setCellValue(String cellValue) {
        this.cellValue = cellValue;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public List<FormatInformation> getCellFormat() {
        return this.formatters;
    }

    public String getSheet() {
        return sheet;
    }


    public String getValue() {
        return this.cellValue;
    }
}
