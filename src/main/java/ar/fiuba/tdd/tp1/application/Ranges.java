package ar.fiuba.tdd.tp1.application;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by jmartty on 11/11/2015.
 */
public class Ranges {

    Hashtable<String, String> ranges;

    public Ranges() {
        this.ranges = new Hashtable<>();
    }

    public void defineRange(String name, String range) {
        ranges.put(name, range);
    }

    public String getRange(String name) {
        return ranges.get(name);
    }

    public boolean rangeExists(String name) {
        return getRanges().contains(name);
    }

    public List<String> getRanges() {
        return new ArrayList<String>(ranges.keySet());
    }

    public String replaceRange(String range) {
        return rangeExists(range) ? getRange(range) : range;
    }

    public String getRangesAsString() {
        return ranges.toString();
    }

    public String replaceAll(String target) {
        String ret = target;
        if (getRanges().size() > 0) {
            for (String range : getRanges()) {
                ret = ret.replace("(" + range + ")", "(" + getRange(range) + ")");
            }
        }
        return ret;
    }

}
