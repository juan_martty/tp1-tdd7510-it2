package ar.fiuba.tdd.tp1.application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;

/**
 * Created by milena on 28/10/15.
 */
public class JsonManager {

    static JsonManager jsonManager;
    private final String path;

    private JsonManager() {
//        this.path = "/home/milena/Escritorio/";
        this.path = System.getProperty("user.dir") + "/" + "output/";
    }

    public static JsonManager getInstance() {
        if (jsonManager == null) {
            jsonManager = new JsonManager();
        }

        return jsonManager;
    }


    public void writeJson(Spreadsheet spreadsheet, String fileName) throws IOException {

        try (Writer writer = new OutputStreamWriter(new FileOutputStream(this.path + fileName) , "UTF-8")) {

            File file = new File(this.path + fileName);
            String version;
            if (file.exists() && !file.isDirectory()) {
                version = "1.2";
                //todo fix
            } else {
                version = "1.1";
            }
            Gson gson = new GsonBuilder().create();
            System.out.println(spreadsheet.getName());
            SpreadSheetInformation spreadSheetInformation = spreadsheet.getInformation(version);
            gson.toJson(spreadSheetInformation, writer);

            writer.close();
        }

    }

    public void readJson(Application application, String fileName) throws Exception {
        //todo falta hacer chequeo
        try (Reader reader = new InputStreamReader(new FileInputStream(this.path + fileName), "UTF-8")) {

            Gson gson = new GsonBuilder().create();
            SpreadSheetInformation spreadSheetInformation = gson.fromJson(reader, SpreadSheetInformation.class);
            application.load(spreadSheetInformation);

        } catch (Exception e) {
            throw e;
        }

    }

}
