package ar.fiuba.tdd.tp1.application;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by milena on 28/10/15.
 */
public class SpreadSheetInformation {

    private final String name;
    private final String version;
    private final List<CellInformation> cellInformations;

    SpreadSheetInformation(String name, String version) {
        this.cellInformations = new ArrayList<>();
        this.name = name;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public Iterator getIterator() {
        return cellInformations.iterator();
    }

    public void addCellInformation(CellInformation cellInformation) {
        this.cellInformations.add(cellInformation);
    }
}
