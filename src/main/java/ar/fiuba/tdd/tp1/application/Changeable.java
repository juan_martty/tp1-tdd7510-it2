package ar.fiuba.tdd.tp1.application;


import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidType;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.WrongSpreadsheetOrSheetUse;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotWhatExpected;

/* Interface for identifying commands that support undo and redo operations.
 */
public interface Changeable {

    void undo(Application application) throws WrongSpreadsheetOrSheetUse, InvalidFormatException;

    void redo(Application application) throws WrongSpreadsheetOrSheetUse, ContentNotWhatExpected,
            InvalidType, InvalidCellLocationFormat, InvalidFormatException;

}
