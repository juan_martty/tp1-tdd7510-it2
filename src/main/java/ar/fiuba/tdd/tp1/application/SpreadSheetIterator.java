package ar.fiuba.tdd.tp1.application;

import ar.fiuba.tdd.tp1.cell.Cell;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by milena on 22/10/15.
 */
public class SpreadSheetIterator {


    private Iterator sheetIter;
    private Sheet actualSheet;
    private SheetIterator cellIterator;

    SpreadSheetIterator(Iterator iter) {
        this.sheetIter = iter;
    }

    public CellInformation next() {

        CellInformation cellInformation = new CellInformation();

        cellInformation.setSheet(this.actualSheet.getName());

        Map.Entry entry = this.cellIterator.next();
        Cell actualCell = (Cell)entry.getValue();

        cellInformation.setContent(actualCell.getContentAsString());
        cellInformation.setCellFormat(actualCell.getFormatt());
        cellInformation.setType(actualCell.getType());
        cellInformation.setId((String) entry.getKey());

        try {
            cellInformation.setCellValue(actualCell.getValueAsString());
        } catch (InvalidSheet |  InvalidFormula | NoSpreadsheetOpen | InvalidCellLocationFormat ex) {
            cellInformation.setCellValue("INVALID!");
        } catch (CellWithoutContent cellWithoutContent) {
            System.out.println();
        } catch (ContentNotDouble contentNotDouble) {
            //TODO QUE PASA SI EL CONTENIDO NO ES DOUBLE?
            contentNotDouble.printStackTrace();
        }
        return cellInformation;

    }

    public Boolean hasNext() {
        if (cellIterator != null && cellIterator.hasNext()) {
            return true;
        }

        if (sheetIter.hasNext()) {
            //se cambia de sheet
            Map.Entry entry = (Map.Entry) sheetIter.next();
            this.actualSheet = (Sheet) entry.getValue();
            this.cellIterator = this.actualSheet.getCSVIterator();
            return this.hasNext();
        } else {
            return false;

        }
    }
}
