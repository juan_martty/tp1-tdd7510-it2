package ar.fiuba.tdd.tp1.application;

import ar.fiuba.tdd.tp1.exceptions.ProblemsWritingFile;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by milena on 22/10/15.
 */
public class CSVManager {

    private static final String DELIMITER = " -> ";
    private static final String FILE_HEADER = "Sheet" + DELIMITER
            + "Cell" + DELIMITER + "VALUE";
    private String currentPath;

    CSVManager() {
        this.currentPath = System.getProperty("user.dir") + "/" + "output";

        //this.currentPath = "/home/milena/Escritorio";
    }

    public void writeCSV(String csvName, Spreadsheet spreadsheet) throws ProblemsWritingFile {

        PrintWriter fileWriter = null;

        try {

            fileWriter = new PrintWriter(this.currentPath + "/" + csvName, "UTF-8");
            fileWriter.println(FILE_HEADER);
            writeContentOf(fileWriter, spreadsheet);

        } catch (IOException e) {
            throw new ProblemsWritingFile();
        } finally {
            closeFile(fileWriter);


        }

    }

    public void writeContentOf(PrintWriter fileWriter, Spreadsheet spreadsheet) throws IOException {
        SpreadSheetIterator spreadSheetIterator = spreadsheet.getIterator();
        while (spreadSheetIterator.hasNext()) {
            System.out.println("hello is it me your looking foor");
            CellInformation cellInformation = spreadSheetIterator.next();
            if (cellInformation.getValue().equals("No Value")) {
                continue;
            }
            String  result = cellInformation.getSheet() + DELIMITER + cellInformation.getId()
                    + DELIMITER + cellInformation.getValue();
            fileWriter.println(result);
//                fileWriter.println(spreadSheetIterator.next(DELIMITER));
            //fileWriter.println(NEW_LINE_SEPARATOR);
        }
    }


    public void closeFile(PrintWriter fileWriter) {
        if (fileWriter != null) {
            fileWriter.flush();
            fileWriter.close();
        }
    }
}
