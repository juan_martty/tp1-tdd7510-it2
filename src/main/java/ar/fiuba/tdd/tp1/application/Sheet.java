package ar.fiuba.tdd.tp1.application;

import ar.fiuba.tdd.tp1.cell.Cell;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidType;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotWhatExpected;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;


/*
 * Sheet represents the collection of cells that live in the same "space".
 * It is identified by name and spreadsheet were it belongs.
 * It is the responsible for creating the necessary cells. They are infinite,
 * so it just stores the ones that were used of asked for by the user.
 */
public class Sheet {

    String name;
    Hashtable<String, Cell> relevantCells;

    public Sheet(String name) {
        this.name = name;
        this.relevantCells = new Hashtable<>();
    }

    public String getName() {
        return this.name;
    }

    public boolean hasCycledReferences() {
        Enumeration<String> keys = relevantCells.keys();
        while (keys.hasMoreElements()) {
            String cellString = keys.nextElement();
            if (relevantCells.get(cellString).hasCycledReference()) {
                return true;
            }
        }
        return false;
    }

    public Cell getCell(CellLocation loc) {
        if (this.relevantCells.containsKey(loc.asString())) {
            return this.relevantCells.get(loc.asString());
        }
        Cell cell = new Cell(loc); //Ver si se crea así o de alguna otra forma.
        this.relevantCells.put(loc.asString(), cell);
        return cell;
    }

    public void write(CellLocation cellLocation, CellContent cellContent) {
        Cell newCell = this.getCell(cellLocation);
        newCell.setContent(cellContent);
    }

    public String getCellValueAsString(CellLocation loc) throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        return this.getCell(loc).getValueAsString();
    }

    public CellContent setCellType(CellLocation loc, String type) throws NoSpreadsheetOpen,
            InvalidSheet, ContentNotWhatExpected, InvalidType, InvalidCellLocationFormat {
        return this.getCell(loc).setType(type);
    }

    public List<String> setCellFormat(CellLocation loc, String type, String format) throws InvalidFormatException {
        return this.getCell(loc).setFormat(type, format);
    }


    public Iterator getIterator() {
        return this.relevantCells.entrySet().iterator();
    }


    public SheetIterator getCSVIterator() {
        return new SheetIterator(getIterator());
    }

}
