package ar.fiuba.tdd.tp1;

/**
 * Utility class, used to reuse code for parsing numbers.
 */
public final class IsDouble {

    /**
     * Utility class should not have a public constructor.
     */
    private IsDouble() {
    }

    /**
     * @param posibleNumber candidate for double-parsing
     * @return if the double-parse is possible or not.
     */
    public static boolean stringIsDouble(final String posibleNumber) {
        boolean isADouble;
        try {
            Double.parseDouble(posibleNumber);
            isADouble = true;
        } catch (NumberFormatException exception) {
            isADouble = false;
        }
        return isADouble;
    }
}
