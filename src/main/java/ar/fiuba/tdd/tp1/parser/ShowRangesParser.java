package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.ShowFile;
import ar.fiuba.tdd.tp1.command.ShowRanges;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;

import java.util.List;


public class ShowRangesParser implements Parser {

    @Override
    public Command parseLine(List<String> parameters) throws AmountOfArgumentsInvalid, InvalidCellLocationFormat {
        return new ShowRanges();
    }
}
