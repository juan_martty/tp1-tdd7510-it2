package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.UndoCommand;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;

import java.util.List;

/**
 * Parser of the command that allows the client to undo an action.
 * Doesn't have to receive any argument.
 */
public class UndoParser implements Parser {

    private static final int NUMBER_OF_PARAMETERS = 1;

    @Override
    public Command parseLine(List<String> parameters) throws AmountOfArgumentsInvalid {
        if (parameters.size() != NUMBER_OF_PARAMETERS) {
            throw new AmountOfArgumentsInvalid();
        }
        return new UndoCommand();
    }

    @Override
    public String getParameters() {
        return "";
    }

}
