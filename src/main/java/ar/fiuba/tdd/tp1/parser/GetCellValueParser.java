package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.GetCellValueCommand;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;

import java.util.List;

/**
 * Parser of the command that allows the client to get the value of a cell.
 * Has to receive the location of the cell.
 * It could fail not only if the amount of arguments isn't the correct one, but also if
 * the format of the cell's location is incorrect.
 */
public class GetCellValueParser implements Parser {

    private static final int NUMBER_OF_PARAMETERS = 2;
    private CellLocationBuilder builder = new CellLocationBuilder();

    @Override
    public Command parseLine(List<String> parameters) throws InvalidCellLocationFormat, AmountOfArgumentsInvalid {
        if (parameters.size() != NUMBER_OF_PARAMETERS) {
            throw new AmountOfArgumentsInvalid();
        }
        String location = parameters.get(1);
        CellLocation cellLocation = builder.build(location);
        return new GetCellValueCommand(cellLocation);
    }

    @Override
    public String getParameters() {
        return getCellLocationParameter();
    }

}
