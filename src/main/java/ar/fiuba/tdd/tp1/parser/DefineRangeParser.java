package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.DefineRange;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;

import java.util.List;

/**
 * Parser of the command that allows the client to get the value of a cell.
 * Has to receive the location of the cell.
 * It could fail not only if the amount of arguments isn't the correct one, but also if
 * the format of the cell's location is incorrect.
 */
public class DefineRangeParser implements Parser {

    private static final int NUMBER_OF_PARAMETERS = 3;

    @Override
    public Command parseLine(List<String> parameters) throws InvalidCellLocationFormat, AmountOfArgumentsInvalid {
        if (parameters.size() != NUMBER_OF_PARAMETERS) {
            throw new AmountOfArgumentsInvalid();
        }
        return new DefineRange(parameters.get(1), parameters.get(2));
    }

    @Override
    public String getParameters() {
        return "<name> <range>";
    }

}
