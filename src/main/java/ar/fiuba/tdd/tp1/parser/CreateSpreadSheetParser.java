package ar.fiuba.tdd.tp1.parser;


import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.CreateSpreadSheetCommand;

/**
 * Parser of the command that simulates the creation of a new spreadsheet.
 * It receives the name of the new spreadsheet.
 */
public class CreateSpreadSheetParser implements Parser {

    public Command createCommand(String parameter) {
        return new CreateSpreadSheetCommand(parameter);
    }

}
