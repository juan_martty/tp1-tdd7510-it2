package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.application.Changeable;
import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.SheetAlredyExists;

import java.util.List;
import java.util.StringJoiner;

/*
 * Interface that specifies the behaviour any Command Parser should have.
 */
public interface Parser {

    CellLocationBuilder builder = new CellLocationBuilder();

    default Command createCommand(String parameter) {
        return new Command() {
            @Override
            public void execute(Application application) throws InvalidSheet, NoSpreadsheetOpen, SheetAlredyExists {

            }

            @Override
            public Changeable getChangeable() {
                return null;
            }
        };
    }

    default Command parseLine(List<String> parameters) throws AmountOfArgumentsInvalid, InvalidCellLocationFormat {
        if (parameters.size() < 2) {
            throw new AmountOfArgumentsInvalid();
        }
        StringJoiner joiner = new StringJoiner(" ");
        for (int i = 1; i < parameters.size(); i++) {
            joiner.add(parameters.get(i));
        }
        return createCommand(joiner.toString());

    }

    default String getCellLocationParameter() {
        return new StringJoiner("").add("<").add(builder.getCellLocationFormat()).add(">").toString();
    }

    default String getParameters() {
        return "<spreadsheetName>";
    }
}

