package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.command.Command;
import ar.fiuba.tdd.tp1.command.RedoCommand;
import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;

import java.util.List;

/**
 * Parser of the command that allows the client to redo an action.
 * Doesn't have to receive any argument.
 */
public class RedoParser implements Parser {
    private static final int NUMBER_OF_PARAMETERS = 1;

    @Override
    public Command parseLine(List<String> parameters) throws AmountOfArgumentsInvalid {
        if (NUMBER_OF_PARAMETERS != parameters.size()) {
            throw new AmountOfArgumentsInvalid();
        }
        return new RedoCommand();
    }

    @Override
    public String getParameters() {
        return "";
    }

}
