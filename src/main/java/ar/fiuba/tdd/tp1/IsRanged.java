package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;

public final class IsRanged {

    /**
     * Utility class should not have a public constructor.
     */
    private IsRanged() {
    }


    /**
     * @param posibleRangedOp candidate for ranged-parsing
     * @return if the Range-parse is possible or not.
     */
    public static boolean stringIsRangedOperation(final String posibleRangedOp) {
        boolean isRangedOperation = false;
        try {
            // function(cellLoc1:CellLoc2) --> function  CellLoc1:CellLoc2
            String[] splittedText = posibleRangedOp.trim().split("[\\(\\)]");
            if (splittedText.length > 2 || !isValidFunction(splittedText[0])) {
                return false;
            }

            if (isValidRange(splittedText[1])) {
                isRangedOperation = true;
            }

        } catch (NumberFormatException exception) {
            isRangedOperation = false;
        }

        return isRangedOperation;
    }

    //Check if string matches "CellLocation:CellLocation"
    private static boolean isValidRange(String rangeAsText) {
        String[] splittedText = rangeAsText.trim().split(":");
        CellLocation initialCell;
        CellLocation finalCell;

        if (splittedText.length > 2) {
            return false;
        }
        try {
            initialCell = new CellLocationBuilder().build(splittedText[0]);
            finalCell = new CellLocationBuilder().build(splittedText[1]);
        } catch (InvalidCellLocationFormat ex) {
            return false;
        }
        return correctCombination(initialCell, finalCell);
    }

    //Check if initialLocation '<' finalLocation && same sheet.
    private static boolean correctCombination(CellLocation iniLoc, CellLocation finLoc) {
        if (!sameShet(iniLoc, finLoc)) {
            return false;
        }

        if (iniLoc.getColumn().compareTo(finLoc.getColumn()) < 0 || iniLoc.getColumn().length() < finLoc.getColumn().length()) {
            return true;
        } else if (iniLoc.getColumn().compareTo(finLoc.getColumn()) == 0) {
            return iniLoc.getRow() < finLoc.getRow();
        }
        return false;
    }

    private static boolean sameShet(CellLocation initialCell, CellLocation finalCell) {
        return (initialCell.getSheet().compareTo(finalCell.getSheet()) == 0);
    }

    private static boolean isValidFunction(String function) {
        //TODO consultar en una tabla estatica (y ampliable) las funciones
        // de rango
        return function.equals("GENERIC_FUNCTION");
    }

}
