package ar.fiuba.tdd.tp1.vision;

/**
 * Interface that defines the behaviour any object that
 * communicates the result of an operation to the user must have.
 */
public interface Vision {
    void show(String expression);
}
