package ar.fiuba.tdd.tp1.exceptions;

/**
 * Exception to be thrown when the client enters a command
 * that does not exists.
 */
public class InvalidCommand extends ApplicationException {
    public InvalidCommand() {
        super("Error: This is not a valid command.");
    }

    public InvalidCommand(String commands) {
        super("Error: This is not a valid command.\nValid commands are:\n" + commands);
    }

}
