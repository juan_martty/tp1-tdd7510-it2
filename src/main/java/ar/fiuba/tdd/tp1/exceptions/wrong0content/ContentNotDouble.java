package ar.fiuba.tdd.tp1.exceptions.wrong0content;

public class ContentNotDouble extends ContentNotWhatExpected {

    public ContentNotDouble() {
        super("Error: The cell content asked for as double can't be transformed to double.");
    }

    public ContentNotDouble(String message) {
        super(message);
    }

}
