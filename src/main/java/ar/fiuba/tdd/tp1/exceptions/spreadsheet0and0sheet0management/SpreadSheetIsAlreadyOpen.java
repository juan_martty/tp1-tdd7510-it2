package ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management;

/**
 * Exception to be raise when the client tries to open a file while one
 * already is open.
 */
public class SpreadSheetIsAlreadyOpen extends WrongSpreadsheetOrSheetUse {
    public SpreadSheetIsAlreadyOpen() {
        super("Error: an spreadsheet has been already open.");
    }
}
