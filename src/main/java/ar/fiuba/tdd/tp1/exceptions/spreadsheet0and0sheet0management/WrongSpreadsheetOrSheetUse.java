package ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management;

import ar.fiuba.tdd.tp1.exceptions.ApplicationException;

public class WrongSpreadsheetOrSheetUse extends ApplicationException {

    public WrongSpreadsheetOrSheetUse(String message) {
        super(message);
    }

}
