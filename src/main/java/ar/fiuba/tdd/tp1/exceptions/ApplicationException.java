package ar.fiuba.tdd.tp1.exceptions;


public class ApplicationException extends Exception {
    public ApplicationException(String message) {
        super(message);
    }
}
