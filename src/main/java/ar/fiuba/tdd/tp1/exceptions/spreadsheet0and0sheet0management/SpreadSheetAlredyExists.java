package ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management;

/**
 * Exception that's thrown when the clients tries to create a spreadsheet that already exists.
 */
public class SpreadSheetAlredyExists extends WrongSpreadsheetOrSheetUse {
    public SpreadSheetAlredyExists() {
        super("Error: A Spreadsheet with that name already exists.");
    }
}
