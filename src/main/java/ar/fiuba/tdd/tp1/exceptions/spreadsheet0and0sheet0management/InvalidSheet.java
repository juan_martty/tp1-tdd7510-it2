package ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management;

/**
 * Exception to be raised when the user try to access a cell from a sheet that does not exist in
 * the opened spreadsheet.
 */
public class InvalidSheet extends WrongSpreadsheetOrSheetUse {
    public InvalidSheet() {
        super("Error: Specified Sheet of cell does not exist.");
    }
}
