package ar.fiuba.tdd.tp1.exceptions.wrong0content;


import ar.fiuba.tdd.tp1.exceptions.ApplicationException;

public class ContentNotWhatExpected extends ApplicationException {
    public ContentNotWhatExpected(String message) {
        super(message);
    }
}
