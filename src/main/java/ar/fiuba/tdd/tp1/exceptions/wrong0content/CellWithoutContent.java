package ar.fiuba.tdd.tp1.exceptions.wrong0content;


public class CellWithoutContent extends ContentNotWhatExpected {
    public CellWithoutContent() {
        super("Error: Cell without content.");
    }
}
