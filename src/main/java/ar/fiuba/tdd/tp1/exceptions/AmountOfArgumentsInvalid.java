package ar.fiuba.tdd.tp1.exceptions;

public class AmountOfArgumentsInvalid extends ApplicationException {
    public AmountOfArgumentsInvalid() {
        super("Error: wrong amount of arguments passed.");
    }
}
