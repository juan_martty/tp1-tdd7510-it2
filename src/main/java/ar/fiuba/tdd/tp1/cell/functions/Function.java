package ar.fiuba.tdd.tp1.cell.functions;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.cell.formatters.FormatInformation;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Function implements CellContent {

    CellSet set;
    RangedOperation operation;
    //Ranges ranges;
    private final String content;

    public Function(RangedOperation op, CellSet set, String content) {
        this.operation = op;
        this.set = set;
        //this.ranges = null;
        this.content = content;
    }

    @Override
    public double getValue() throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        return operation.getValue(this.set);
    }

    @Override
    public String getValueAsString() throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {
        return operation.getValueAsString(this.set, this.content);
    }

    @Override
    public void completeContent(Application app) throws NoSpreadsheetOpen, InvalidSheet {
        this.set.completeContent(app);
        // Save reference to named ranges
        //this.ranges = app.getWorkingSpreadSheet().ranges;
    }

    @Override
    public String getType() {
        return "Function";
    }

    @Override
    public String getContent() {
        return this.content;
    }

    @Override
    public ArrayList<FormatInformation> getFormatt() {

        return null;
    }

    @Override
    public CellContent getCopy() {
        return new Function(this.operation, this.set, this.content);
    }

    public static String[] splitFunctionArgs(String content) {
        Matcher matcher = Pattern.compile("^\\s*([A-Z]+)\\((.*)\\)$").matcher(content);
        return matcher.group(2).split(",");
    }

}
