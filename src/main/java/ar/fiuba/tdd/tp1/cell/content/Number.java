package ar.fiuba.tdd.tp1.cell.content;


import ar.fiuba.tdd.tp1.cell.formatters.DecimalFormatter;
import ar.fiuba.tdd.tp1.cell.formatters.FormatInformation;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Represents the behaviour of the cell when
 * containing no more than a number.
 */
public class Number implements CellContent {
    private double value;
    private DecimalFormatter formatter;

    public Number(double number) {
        this.value = number;
        this.formatter = new DecimalFormatter();
    }

    private Number(double number, DecimalFormatter formatter) {
        this.value = number;
        this.formatter = formatter;
    }

    @Override
    public List<String> setFormat(String type, String format) throws InvalidFormatException {
        if (type.toLowerCase().equals("decimal")) {
            return Arrays.asList("decimal", this.formatter.setFormat(format));
        } else {
            throw new InvalidFormatException("Only valid formatter is decimal.");
        }
    }

    @Override
    public String getContent() {
        return Double.toString(this.value);
    }

    @Override
    public ArrayList getFormatt() {
        if (this.formatter.getFormat() == null) {
            return null;
        }
        ArrayList<FormatInformation> aux = new ArrayList<>();
        aux.add(this.formatter.getFormat());
        return aux;
    }

    @Override
    public double getValue() throws CellWithoutContent {
        return this.value;
    }

    @Override
    public String getValueAsString() {
        return this.formatter.getFormatted(Double.toString(this.value));
    }

    @Override
    public String getType() {
        return "Number";
    }

    @Override
    public CellContent getCopy() {
        return new Number(this.value, this.formatter);
    }

}
