package ar.fiuba.tdd.tp1.cell.content;


import ar.fiuba.tdd.tp1.cell.formatters.DecimalFormatter;
import ar.fiuba.tdd.tp1.cell.formatters.FormatInformation;
import ar.fiuba.tdd.tp1.cell.formatters.SymbolFormatter;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Money implements CellContent {

    double value;
    SymbolFormatter symbol;
    DecimalFormatter decimals;

    public Money(double value) {
        this.value = value;
        this.symbol = null;
        this.decimals = new DecimalFormatter();
    }

    private Money(double value, SymbolFormatter symbol, DecimalFormatter decimal) {
        this.value = value;
        this.symbol = symbol;
        this.decimals = decimal;
    }

    @Override
    public double getValue() throws CellWithoutContent, InvalidFormula, ContentNotDouble {
        return this.value;
    }

    @Override
    public String getValueAsString() throws CellWithoutContent, ContentNotDouble {
        if (this.symbol != null) {
            return this.symbol.getFormatted(this.decimals.getFormatted(Double.toString(this.value)));
        } else {
            return this.decimals.getFormatted(Double.toString(this.value));
        }
    }

    @Override
    public String getType() {
        return "Currency";
    }

    @Override
    public List<String> setFormat(String type, String format) throws InvalidFormatException {
        if (type.toLowerCase().equals("decimal")) {
            ArrayList<String> array = new ArrayList<String>();
            array.add("decimal");
            array.add(this.decimals.setFormat(format));
            return array;
        } else if (type.toLowerCase().equals("symbol")) {
            String oldFormat;
            if (this.symbol == null) {
                this.symbol = new SymbolFormatter(format);
                oldFormat = "";
            } else {
                oldFormat = this.symbol.setFormat(format);
            }
            return Arrays.asList("symbol", oldFormat);
        } else {
            throw new InvalidFormatException("Not a valid formatter.");
        }
    }

    @Override
    public String getContent() {
        if (true) {

            return Double.toString(this.value);
        }
        return null; //Solo hecho para que el cpd no chille
    }

    @Override
    public ArrayList<FormatInformation> getFormatt() {
        ArrayList<FormatInformation> aux = new ArrayList<>();
        if (symbol != null) {
            aux.add(symbol.getFormat());
        }
        if (decimals.getFormat() != null) {
            aux.add(decimals.getFormat());
        }
        return aux;
    }
//    public FormatInformation getFormat() {
//        if (symbol != null) {
//            return this.symbol.getFormat();
//        } else {
//            return this.decimals.getFormat();
//        }
//    }

    @Override
    public CellContent getCopy() {
        return new Money(this.value, this.symbol, this.decimals);
    }

}
