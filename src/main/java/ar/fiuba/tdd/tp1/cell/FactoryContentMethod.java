package ar.fiuba.tdd.tp1.cell;

import ar.fiuba.tdd.tp1.cell.content.CellContent;

/**
 * Factory Method pattern.
 * This is the interface any creator of CellContent must have.
 */
public interface FactoryContentMethod {
    /**
     * @param formula Passed by WriteCellCommand Command.
     * @return a valid CellContent
     */
    CellContent createContent(String formula);
}
