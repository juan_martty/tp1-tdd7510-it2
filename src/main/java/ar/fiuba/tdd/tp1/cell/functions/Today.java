package ar.fiuba.tdd.tp1.cell.functions;

import ar.fiuba.tdd.tp1.cell.content.CellReference;
import ar.fiuba.tdd.tp1.cell.formatters.DateFormatter;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Today implements RangedOperation {

    @Override
    public String getValueAsString(CellSet set, String content) throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        DateFormatter dateFormatter = new DateFormatter();
        return dateFormatter.getFormatted(dateFormat.format(date));
    }

    @Override
    public double getValue(CellSet set) throws ContentNotDouble {
        throw new ContentNotDouble("TextResult");
    }

    @Override
    public double getResult() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent, ContentNotDouble, InvalidFormula {
        throw new ContentNotDouble("TextResult");
    }

    @Override
    public void setResult(CellReference ref) {
    }

}