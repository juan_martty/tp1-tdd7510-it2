package ar.fiuba.tdd.tp1.cell.formatters;


import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;

public interface Formatter {

    public String setFormat(String symbol) throws InvalidFormatException;

    public String getFormatted(String value);

    public FormatInformation getFormat();


}
