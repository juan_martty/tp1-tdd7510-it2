package ar.fiuba.tdd.tp1.cell.formatters;


public class SymbolFormatter implements Formatter {

    private String symbol;

    public SymbolFormatter() {
        this.symbol = "";
    }

    public SymbolFormatter(String symbol) {
        this.symbol = symbol;
    }

    public String setFormat(String symbol) {
        String aux = this.symbol;
        this.symbol = symbol;
        return aux;
    }

    public String getFormatted(String value) {
        return this.symbol + " " + value;
    }

    @Override
    public FormatInformation getFormat() {
        return new FormatInformation(this.symbol);
    }

}
