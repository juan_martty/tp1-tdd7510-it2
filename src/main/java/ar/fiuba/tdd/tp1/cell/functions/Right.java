package ar.fiuba.tdd.tp1.cell.functions;

import ar.fiuba.tdd.tp1.cell.content.CellReference;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;


public class Right implements RangedOperation {

    @Override
    public String getValueAsString(CellSet set, String content) throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {

        CellReference fst = set.next();
        return fst.getValueAsString().substring(RangedOperation.getSubstringIndex(content));
    }

    @Override
    public double getResult() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent, ContentNotDouble, InvalidFormula {
        throw new ContentNotDouble("TextResult");
    }

    @Override
    public void setResult(CellReference ref) {
    }

    @Override
    public double getValue(CellSet set) throws ContentNotDouble {
        throw new ContentNotDouble("TextResult");
    }

}