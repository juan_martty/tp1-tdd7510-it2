package ar.fiuba.tdd.tp1.cell.formatters;

import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DateFormatter implements Formatter {

    private String format;
    private List<String> possibleFormats;

    public DateFormatter() {
        this.possibleFormats = new ArrayList<>();
        this.possibleFormats.add("yyyy-mm-dd");
        this.possibleFormats.add("dd-mm-yyyy");
        this.possibleFormats.add("mm-dd-yyyy");
        this.format = "dd-mm-yyyy";
    }

    public String setFormat(String format) throws InvalidFormatException {
        if (this.possibleFormats.contains(format.toLowerCase())) {
            String aux = this.format;
            this.format = format.toLowerCase();
            return aux;
        } else {
            throw new InvalidFormatException("Not a valid date format");
        }
    }

    public String getFormatted(String localDate) {
        String pattern = "yyyy-MM-dd";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        LocalDate value = LocalDate.parse(localDate, formatter);
        return value.format(DateTimeFormatter.ofPattern(this.format.replace("m", "M")));
    }

    public FormatInformation getFormat() {
        return new FormatInformation(this.format);
    }

}
