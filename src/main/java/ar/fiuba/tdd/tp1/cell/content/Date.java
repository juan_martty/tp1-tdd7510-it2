package ar.fiuba.tdd.tp1.cell.content;


import ar.fiuba.tdd.tp1.cell.formatters.DateFormatter;
import ar.fiuba.tdd.tp1.cell.formatters.FormatInformation;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Date implements CellContent {

    private final String content;
    LocalDate date;
    DateFormatter formatter;


    public Date(LocalDate date, String content) {
        this.date = date;
        this.content = content;
        this.formatter = new DateFormatter();
    }

    public double getValue() throws ContentNotDouble {
        throw new ContentNotDouble();
    }

    @Override
    public String getValueAsString() throws CellWithoutContent, ContentNotDouble {
        return this.formatter.getFormatted(this.date.toString());
    }

    @Override
    public String getType() {
        return "Date";
    }

    @Override
    public List<String> setFormat(String type, String format) throws InvalidFormatException {
        if (!type.toLowerCase().equals("format")) {
            throw new InvalidFormatException("Not a valid formatter.");
        } else {
            return Arrays.asList("format", this.formatter.setFormat(format));
        }
    }

    @Override
    public String getContent() {
        return this.content;
    }

    @Override
    public ArrayList getFormatt() {
        ArrayList<FormatInformation> aux = new ArrayList<>();
        aux.add(this.formatter.getFormat());
        return aux;

    }

    @Override
    public CellContent getCopy() {
        return new Date(this.date.minusDays(0), this.content);
    }

}
