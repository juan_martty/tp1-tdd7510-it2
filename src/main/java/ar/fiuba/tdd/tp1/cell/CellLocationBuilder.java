package ar.fiuba.tdd.tp1.cell;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;

import java.util.Arrays;
import java.util.List;


/**
 * It parses the location of a cell expecting the format:
 * "Sheet.Row-Column", where Row is a number and Column any set of letters;
 * to create finally the CellLocation object.
 */
public class CellLocationBuilder {

    public CellLocationBuilder() {

    }

    private static boolean isAlpha(String name) {
        return name.matches("[a-zA-Z]+");
    }

    private static boolean isNumber(String name) {
        return name.matches("[0-9]+");
    }

    public CellLocation build(String location) throws InvalidCellLocationFormat {

        List<String> aux = Arrays.asList(location.split("\\."));
        if (aux.size() != 2) {
            throw new InvalidCellLocationFormat();
        } else {
            List<String> cellLocation = Arrays.asList(aux.get(1).split("-"));
            if ((cellLocation.size() != 2) || (!isNumber(cellLocation.get(0))) || (!isAlpha(cellLocation.get(1)))) {
                throw new InvalidCellLocationFormat();
            }
            return new CellLocation(aux.get(0), Integer.parseInt(cellLocation.get(0)), cellLocation.get(1).toLowerCase());
        }
    }

    public String getCellLocationFormat() {
        return "SheetName.Number-Letters";
    }

}
