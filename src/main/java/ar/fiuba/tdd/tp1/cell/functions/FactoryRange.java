package ar.fiuba.tdd.tp1.cell.functions;

import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import ar.fiuba.tdd.tp1.cell.content.CellReference;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;

public class FactoryRange {

    public FactoryRange() {
    }

    public Range build(String range) {
        String[] splittedLocations = range.trim().split(":");
        CellLocation initialCell = null;
        CellLocation finalCell = null;
        try {
            initialCell = new CellLocationBuilder().build(splittedLocations[0]);
            finalCell = new CellLocationBuilder().build(splittedLocations[1]);
        } catch (InvalidCellLocationFormat ex) {
            System.out.println("Acá hay que hacer algo pero no me pasa el CMD si no pongo algo.");
            //TODO
        }
        CellReference initialReference = new CellReference(initialCell);
        CellReference finalReference = new CellReference(finalCell);
        Range rangeObject = new Range(initialReference, finalReference);

        //TODO: ampliar.
        return rangeObject;
    }
}
