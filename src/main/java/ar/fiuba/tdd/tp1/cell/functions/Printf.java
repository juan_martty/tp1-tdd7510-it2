package ar.fiuba.tdd.tp1.cell.functions;

import ar.fiuba.tdd.tp1.cell.content.CellReference;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;


public class Printf implements RangedOperation {

    @Override
    public double getValue(CellSet set) throws ContentNotDouble {
        throw new ContentNotDouble("TextResult");
    }

    @Override
    public double getResult() throws NoSpreadsheetOpen, InvalidSheet, CellWithoutContent, ContentNotDouble, InvalidFormula {
        throw new ContentNotDouble("TextResult");
    }

    @Override
    public String getValueAsString(CellSet set, String content) throws NoSpreadsheetOpen, InvalidSheet,
            CellWithoutContent, ContentNotDouble, InvalidFormula, InvalidCellLocationFormat {

        String driver = RangedOperation.getArgs(content).split(",")[0];
        int index = 0;
        while (set.hasNext()) {
            CellReference cell = set.next();
            driver = driver.replace("$" + Integer.toString(index), cell.getValueAsString());
            index++;
        }
        return driver.replace("\"", "");
    }

    @Override
    public void setResult(CellReference ref) {
    }

}