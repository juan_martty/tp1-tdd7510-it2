package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.ConcreteDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersistenceTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new ConcreteDriver();
    }

    @Test
    public void persistOneWorkBookAndRetrieveIt() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 10");
        testDriver.setCellValue("tecnicas", "default", "A2", "= A1 + 1");
        testDriver.setCellValue("tecnicas", "default", "A3", "= A2 + 1");
        testDriver.setCellValue("tecnicas", "default", "A4", "= A3 + 1");
        testDriver.setCellValue("tecnicas", "default", "A5", "= A4 + 1");
        testDriver.persistWorkBook("tecnicas", "proy1.jss");
        testDriver.setCellValue("tecnicas", "default", "A1", "2");
        testDriver.setCellValue("tecnicas", "default", "A2", "2");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");
        testDriver.reloadPersistedWorkBook("proy1.jss");

        assertEquals(10 + 1 + 1 + 1 + 1, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }


}

