package ar.fiuba.tdd.tp1.cell;

import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotWhatExpected;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FactoryContentTest {
    FactoryContent factory = new FactoryContent();

    @Test
    public void testCreateContentWithNumber1() throws Exception {
        CellContent content = factory.createContent("5");
        assertEquals(5, content.getValue(), 0);
    }

    @Test
    public void testCreateContentWithNumber2() throws Exception {
        CellContent content = factory.createContent("-10");
        assertEquals(-10, content.getValue(), 0);
    }

    @Test
    public void testCreateContentWithNumber3() throws Exception {
        CellContent content = factory.createContent("-5.5");
        assertEquals(-5.5, content.getValue(), 0);
    }

    @Test
    public void testCreateContentWithTextContent() throws Exception {
        CellContent content = factory.createContent("*invalid stuff");
        assertEquals("*invalid stuff", content.getValueAsString());
    }

    @Test
    public void testCreateTextWithOnlyNumbers() throws Exception {
        CellContent content = factory.createContent("9 83 47 6");
        assertEquals("9 83 47 6", content.getValueAsString());
    }

    @Test
    public void testCreateTextWithFormulaWithoutEqual() throws Exception {
        CellContent content = factory.createContent("9 + 3 - 47 - 6");
        assertEquals("9 + 3 - 47 - 6", content.getValueAsString());
    }

    @Test
    public void testSimpleSumWithNumbers() throws Exception {
        CellContent content = factory.createContent("=9 + 8");
        assertEquals(17, content.getValue(), 0);
    }

    @Test
    public void testSimpleSubtractionWithNumbers() throws Exception {
        CellContent content = factory.createContent("= 3    - 10");
        assertEquals(-7, content.getValue(), 0);
    }

    @Test
    public void testSimpleSumWithNegativeNumbers() throws Exception {
        CellContent content = factory.createContent("=-8 + -10");
        assertEquals(-18, content.getValue(), 0);
    }

    @Test
    public void testSimpleSubtractionWithNegativeNumbers() throws Exception {
        CellContent content = factory.createContent("=-8 -   -10");
        assertEquals(2, content.getValue(), 0);
    }

    @Test
    public void testSimpleSubtractionWithNegativeAndPositiveNumbers() throws Exception {
        CellContent content = factory.createContent("=-8 - 3");
        assertEquals(-11, content.getValue(), 0);
    }

    @Test
    public void testLongFormulaWithSums() throws Exception {
        CellContent content = factory.createContent("=         1 + 2   + 3");
        assertEquals(6, content.getValue(), 0);
    }

    @Test
    public void testSimpleLongFormulas() throws Exception {
        CellContent content = factory.createContent("=1 + 2 - 6");
        assertEquals(-3, content.getValue(), 0);
    }

    @Test
    public void testLongFormula() throws Exception {
        CellContent content = factory.createContent("=-1 +  2 - 6 +    3  - 2");
        assertEquals(-4, content.getValue(), 0);
    }

    @Test
    public void testAnotherLongFormula() throws Exception {
        CellContent content = factory.createContent("=10 - 1 - 2 - 3 - 4");
        assertEquals(0, content.getValue(), 0);
    }

    @Test
    public void testLongFormulaWithBigNegatives() throws Exception {
        CellContent content = factory.createContent("=8 - 1 + 3 + 5 - -100 + 1");
        assertEquals(116, content.getValue(), 0);
    }

    @Test
    public void testCreateCorrectFormulaWithReference() throws Exception {
        CellContent content = factory.createContent("=8 + sheet3.5-a - 6");
    }

    @Test(expected = ContentNotDouble.class)
    public void testCreateFormulaContentWithInvalidReferenceThrowsExceptionWhenAskedValue() throws Exception {
        CellContent content = factory.createContent("=sheet2.hdo-9h");
        content.getValue();

    }

    @Test(expected = ContentNotDouble.class)
    public void testInvalidFormula() throws Exception {
        CellContent content = factory.createContent("=8 + 5 - ahReLoco");
        content.getValue();
    }

    @Test(expected = ContentNotDouble.class)
    public void testNoFormula() throws Exception {
        CellContent content = factory.createContent("=estoNoEsUnaFórmula!");
        content.getValue();
    }

    @Test(expected = InvalidFormula.class)
    public void testFormulaMissingASecondOperand() throws Exception {
        CellContent content = factory.createContent("=5 +    7 - ");
        content.getValue();
    }

    @Test(expected = InvalidFormula.class)
    public void testFormulaMissingAMiddleOperand() throws Exception {
        CellContent content = factory.createContent("=5 +   - 5 ");
        content.getValue();
    }

    @Test
    public void testFormulaWithOnlyOneOperand() throws Exception {
        CellContent content = factory.createContent("=7");
        assertEquals(7, content.getValue(), 0);
    }

    @Test(expected = InvalidFormula.class)
    public void testFormulaStartingWithOperator() throws Exception {
        CellContent content = factory.createContent("=- 7");
        content.getValue();
    }

    @Test(expected = InvalidFormula.class)
    public void testFormulaWithInexistentOperator() throws Exception {
        CellContent content = factory.createContent("=9             MOD 7");
        content.getValue();
    }

    @Test
    public void testEmptyString() throws Exception {
        CellContent content = factory.createContent("");
        assertEquals("", content.getValueAsString());
    }

    @Test
    public void testStringWithOnlySpaces() throws Exception {
        CellContent content = factory.createContent("      ");
        assertEquals("      ", content.getValueAsString());
    }

    @Test
    public void testEmptyFormulaNoException() throws Exception {
        CellContent content = factory.createContent("=      ");
        assertNotNull(content);
    }

    @Test(expected = ContentNotWhatExpected.class)
    public void testEmptyFormulaExceptionWhenRead() throws Exception {
        CellContent content = factory.createContent("=      ");
        content.getValue();
    }

}