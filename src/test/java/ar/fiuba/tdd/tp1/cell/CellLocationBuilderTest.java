package ar.fiuba.tdd.tp1.cell;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test created to prove the proper functionality of the cell's location builder.
 */
public class CellLocationBuilderTest {

    private CellLocationBuilder builder;

    @Before
    public void setUp() {
        builder = new CellLocationBuilder();
    }

    @Test(expected = InvalidCellLocationFormat.class)
    public void notPoint() throws Exception {
        builder.build("sheet1a-1");
    }

    @Test(expected = InvalidCellLocationFormat.class)
    public void onlyPoint() throws Exception {
        builder.build(".");
    }

    @Test(expected = InvalidCellLocationFormat.class)
    public void severalPoints() throws Exception {
        builder.build("sheet1.a.2");
    }

    @Test(expected = InvalidCellLocationFormat.class)
    public void invalidFormat() throws Exception {
        builder.build("sheet1.a2");
    }

    @Test(expected = InvalidCellLocationFormat.class)
    public void rowHasLetter() throws Exception {
        builder.build("sheet1.2a-a");
    }

    @Test(expected = InvalidCellLocationFormat.class)
    public void columHasNumber() throws Exception {
        builder.build("sheet1.2-a2");
    }

    @Test
    public void correctLocation() throws Exception {
        assertEquals(builder.build("sheet3.17-a").asString(), "sheet3.17-a");
    }

    @Test
    public void anotherCorrectLocation() throws Exception {
        assertEquals(builder.build("sheet3otravez.123-abbcd").asString(), "sheet3otravez.123-abbcd");
    }

}
