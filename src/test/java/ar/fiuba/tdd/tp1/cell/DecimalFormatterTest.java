package ar.fiuba.tdd.tp1.cell;


import ar.fiuba.tdd.tp1.cell.formatters.DecimalFormatter;


import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DecimalFormatterTest {

    DecimalFormatter formatter;

    @Before
    public void setUp() {
        formatter = new DecimalFormatter();
    }

    @Ignore
    @Test
    public void testIntegerWithNoDecimalsSpecified() {
        assertEquals("3", formatter.getFormatted("3"));
    }

    @Ignore
    @Test
    public void testDoubleWithNoDecimalsSpecified() {
        assertEquals("3.50", formatter.getFormatted("3.5"));
    }

    @Ignore
    @Test
    public void testIntegerWithThreeDecimalsSpecified() throws Exception {
        formatter.setFormat("3");
        assertEquals("3.000", formatter.getFormatted("3"));
    }

    @Ignore
    @Test
    public void testDoubleWithFiveDecimalsAndThreeDecimalsSpecified() throws Exception {
        formatter.setFormat("3");
        assertEquals("3.457", formatter.getFormatted("3.45678"));
    }

    @Ignore
    @Test
    public void testDoubleWithTwoDecimalsAndThreeDecimalsSpecified() throws Exception {
        formatter.setFormat("3");
        assertEquals("3.450", formatter.getFormatted("3.45"));
    }

}
