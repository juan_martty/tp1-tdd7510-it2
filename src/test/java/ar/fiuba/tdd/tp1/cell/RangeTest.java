package ar.fiuba.tdd.tp1.cell;

import ar.fiuba.tdd.tp1.IsRanged;
import ar.fiuba.tdd.tp1.cell.content.CellReference;
import ar.fiuba.tdd.tp1.cell.functions.Range;
import ar.fiuba.tdd.tp1.exceptions.InvalidCellLocationFormat;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RangeTest {

    @Test
    public void validRangeSameRow() throws InvalidCellLocationFormat {
        String text = "GENERIC_FUNCTION(hoja1.76-a:hoja1.76-e)";
        assertEquals(true, IsRanged.stringIsRangedOperation(text));
        CellReference start = new CellReference(new CellLocationBuilder().build("hoja1.76-a"));
        CellReference end = new CellReference(new CellLocationBuilder().build("hoja1.76-e"));

        Range range = new Range(start, end);
        int count = 0;
        while (range.hasNext()) {
            range.next();
            count++;
        }
        assertEquals(5, count, 0);
    }

    @Test
    public void validRangeSameRowMultipleColumns() throws InvalidCellLocationFormat {
        String text = "GENERIC_FUNCTION(hoja1.76-a:hoja1.76-cv)";
        assertEquals(true, IsRanged.stringIsRangedOperation(text));
        CellReference start = new CellReference(new CellLocationBuilder().build("hoja1.76-a"));
        CellReference end = new CellReference(new CellLocationBuilder().build("hoja1.76-cv"));

        Range range = new Range(start, end);
        int count = 0;
        while (range.hasNext()) {
            range.next();
            count++;
        }
        assertEquals(100, count, 0);
    }

    @Test
    public void validRangeDifferentRowMultipleColumns() throws InvalidCellLocationFormat {
        String text = "GENERIC_FUNCTION(hoja1.76-a:hoja1.80-cv)";
        assertEquals(true, IsRanged.stringIsRangedOperation(text));

        CellReference start = new CellReference(new CellLocationBuilder().build("hoja1.76-a"));
        CellReference end = new CellReference(new CellLocationBuilder().build("hoja1.80-cv"));

        Range range = new Range(start, end);
        int count = 0;

        while (range.hasNext()) {
            range.next();
            count++;
        }
        assertEquals(500, count, 0);
    }

    @Test
    public void validRangeDifferentRowOneColumn() throws InvalidCellLocationFormat {
        String text = "GENERIC_FUNCTION(hoja1.75-hola:hoja1.100-hola)";
        assertEquals(true, IsRanged.stringIsRangedOperation(text));

        CellReference start = new CellReference(new CellLocationBuilder().build("hoja1.75-hola"));
        CellReference end = new CellReference(new CellLocationBuilder().build("hoja1.100-hola"));

        Range range = new Range(start, end);
        int count = 0;

        while (range.hasNext()) {
            range.next();
            count++;
        }
        assertEquals(26, count, 0);
    }

}
