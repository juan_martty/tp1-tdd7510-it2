package ar.fiuba.tdd.tp1.cell;

import ar.fiuba.tdd.tp1.cell.content.CellReference;
import ar.fiuba.tdd.tp1.cell.content.Formula;
import ar.fiuba.tdd.tp1.cell.content.Number;
import ar.fiuba.tdd.tp1.cell.content.Text;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.ContentNotDouble;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.InvalidFormula;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BuilderFormulaTest {

    BuilderFormula builder;
    ar.fiuba.tdd.tp1.cell.content.Number number1;
    Number number2;
    Text text;
    CellReference reference;

    @Before
    public void setUp() {
        System.out.println("\n\nEntró");
        builder = new BuilderFormula();
        System.out.println("Builder");
        number1 = new Number(7);
        System.out.println("Number1");
        number2 = new Number(-70);
        System.out.println("Number2");
        text = new Text("texto");
        System.out.println("Text");
        reference = new CellReference(new CellLocation("sheet", 6, "AA"));
        System.out.println("Reference\n\n");
    }

    @Test(expected = InvalidFormula.class)
    public void testGetFormulaFromNothingReturnsEmptyFormula() throws Exception {
        Formula form = builder.getFormula();
        form.getValue();
    }

    @Test
    public void testAddOneNumberOperand() throws Exception {
        builder.addOperand(number1);
        Formula formula = builder.getFormula();
        assertEquals(7, formula.getValue(), 0);
    }

    @Test(expected = InvalidFormula.class)
    public void testTwoOperandsNoOperator() throws Exception {
        builder.addOperand(number1);
        builder.addOperand(text);
        Formula formula = builder.getFormula();
        formula.getValue();
    }

    @Test(expected = InvalidFormula.class)
    public void testThreeOperandsNoFirstOperator() throws Exception {
        builder.addOperand(number1);
        builder.addOperand(text);
        builder.addOperator((op1, op2) -> op1);
        builder.addOperand(reference);
        Formula formula = builder.getFormula();
        formula.getValue();
    }

    @Test
    public void testCorrectFormulaWithTwoOperands() throws Exception {
        builder.addOperand(number2);
        builder.addOperator((op1, op2) -> op1);
        builder.addOperand(number1);
        Formula formula = builder.getFormula();
        assertEquals(-70, formula.getValue(), 0);
    }

    @Test
    public void testCorrectFormulaWithFiveOperands() throws Exception {
        builder.addOperand(number1);
        builder.addOperator((op1, op2) -> op1 + op2);
        builder.addOperand(number1);
        builder.addOperator((op1, op2) -> op1 - op2);
        builder.addOperand(number1);
        builder.addOperator((op1, op2) -> op1 + op2);
        builder.addOperand(number2);
        builder.addOperator((op1, op2) -> op1 - op2);
        builder.addOperand(number2);
        Formula formula = builder.getFormula();
        assertEquals(7, formula.getValue(), 0);
    }

    @Test(expected = ContentNotDouble.class)
    public void testCorrectWithText() throws Exception {
        builder.addOperand(number1);
        builder.addOperator((op1, op2) -> op1);
        builder.addOperand(text);
        Formula formula = builder.getFormula();
        formula.getValue();
    }

    @Test(expected = InvalidFormula.class)
    public void testNoFirstOperand() throws Exception {
        builder.addOperator((op1, op2) -> op1);
        builder.addOperand(reference);
        Formula formula = builder.getFormula();
        formula.getValue();
    }

    @Test(expected = InvalidFormula.class)
    public void testTwoConsecutiveOperands() throws Exception {
        builder.addOperand(number1);
        builder.addOperator((op1, op2) -> op1 + op2);
        builder.addOperator((op1, op2) -> op1 - op2);
        builder.addOperand(reference);
        Formula formula = builder.getFormula();
        formula.getValue();
    }

    @Test(expected = InvalidFormula.class)
    public void testInvalidFormulaButIncompleteReferenceFirst() throws Exception {
        builder.addOperand(reference);
        builder.addOperator((op1, op2) -> op1 + op2);
        builder.addOperator((op1, op2) -> op1 - op2);
        builder.addOperand(text);
        Formula formula = builder.getFormula();
        formula.getValue();
    }

    @Test(expected = InvalidFormula.class)
    public void testIncorrectFormulaButTextFirst() throws Exception {
        builder.addOperand(text);
        builder.addOperator((op1, op2) -> op1 + op2);
        builder.addOperator((op1, op2) -> op1 - op2);
        builder.addOperand(reference);
        Formula formula = builder.getFormula();
        formula.getValue();
    }

}