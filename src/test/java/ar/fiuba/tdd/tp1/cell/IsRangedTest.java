package ar.fiuba.tdd.tp1.cell;

import ar.fiuba.tdd.tp1.IsRanged;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IsRangedTest {

    @Test
    public void validRangeSameRow() {
        String text = "GENERIC_FUNCTION(hoja1.76-a:hoja1.76-z)";
        assertEquals(true, IsRanged.stringIsRangedOperation(text));
    }

    @Test
    public void validRangeSameColumn() {
        String text = "GENERIC_FUNCTION(test.5-zzz:test.10-zzz)";
        assertEquals(true, IsRanged.stringIsRangedOperation(text));
    }

    @Test
    public void validRangeValidSquare() {
        String text = "GENERIC_FUNCTION(test.1-a:test.100-z)";
        assertEquals(true, IsRanged.stringIsRangedOperation(text));
    }

    @Test
    public void validRangeMultipleColumns() {
        String text = "GENERIC_FUNCTION(test.1-a:test.100-ab)";
        assertEquals(true, IsRanged.stringIsRangedOperation(text));
    }

    @Test
    public void validRangeMultipleColumnsDiferentLength() {
        String text = "GENERIC_FUNCTION(test.1-z:test.100-aa)";
        String text2 = "GENERIC_FUNCTION(test.1-az:test.100-aza)";
        assertEquals(true, IsRanged.stringIsRangedOperation(text));
        assertEquals(true, IsRanged.stringIsRangedOperation(text2));
    }

    @Test
    public void invalidRangeInvalidSquare() {
        String text = "GENERIC_FUNCTION(test.100-z:test.1-a)";
        assertEquals(false, IsRanged.stringIsRangedOperation(text));
    }
}
