package ar.fiuba.tdd.tp1.application;

import ar.fiuba.tdd.tp1.cell.Cell;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import ar.fiuba.tdd.tp1.cell.FactoryContent;
import ar.fiuba.tdd.tp1.cell.content.Number;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.wrong0content.CellWithoutContent;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class ApplicationTest {

    Application application;
    CellLocationBuilder locBuilder;

    @Before
    public void setUp() {
        application = new Application();
        locBuilder = new CellLocationBuilder();
    }

    @Test
    public void addSpreadsheet() {
        assertEquals(application.addSpreadSheet("SS1"), true);
        assertEquals(application.addSpreadSheet("SS1"), false);
        assertEquals(application.addSpreadSheet("SS2"), true);
        assertEquals(application.addSpreadSheet("SS1"), false);
    }

    @Test
    public void isASpreadsheetOpen() {
        assertEquals(application.isASpreadSheetOpen(), false);
        assertEquals(application.addSpreadSheet("SS1"), true);
        assertEquals(application.isASpreadSheetOpen(), false);
        assertEquals(application.openSpreadSheet("SS1"), true);
        assertEquals(application.isASpreadSheetOpen(), true);
    }

    @Test
    public void openSpreadsheet() {
        assertEquals(application.openSpreadSheet("SS1"), false);
        assertEquals(application.addSpreadSheet("SS1"), true);
        assertEquals(application.openSpreadSheet("SS1"), true);
        assertEquals(application.openSpreadSheet("SS2"), false);
        assertEquals(application.addSpreadSheet("SS2"), true);
        assertEquals(application.openSpreadSheet("SS1"), false);
        assertEquals(application.openSpreadSheet("SS2"), false);
        assertEquals(application.closeSpreadSheet(), true);
        assertEquals(application.openSpreadSheet("SS2"), true);
        assertEquals(application.openSpreadSheet("SS1"), false);
        assertEquals(application.closeSpreadSheet(), true);
        assertEquals(application.openSpreadSheet("SS1"), true);
        assertEquals(application.openSpreadSheet("SS2"), false);
    }

    @Test
    public void getWorkingSpreadsheet() {
        Spreadsheet ss = application.getWorkingSpreadSheet();
        assertEquals(ss, null);
        assertEquals(application.addSpreadSheet("SS1"), true);
        ss = application.getWorkingSpreadSheet();
        assertEquals(ss, null);
        assertEquals(application.openSpreadSheet("SS1"), true);
        ss = application.getWorkingSpreadSheet();
        assertNotEquals(ss, null);
        assertEquals(ss.getName(), "SS1");
    }

    @Test
    public void existsSpreadsheet() {
        assertEquals(application.existsSpreadSheet("SS1"), false);
        assertEquals(application.addSpreadSheet("SS1"), true);
        assertEquals(application.existsSpreadSheet("SS1"), true);
        assertEquals(application.existsSpreadSheet("SS2"), false);
        assertEquals(application.addSpreadSheet("SS2"), true);
        assertEquals(application.existsSpreadSheet("SS1"), true);
        assertEquals(application.existsSpreadSheet("SS2"), true);
    }

    @Test
    public void closeSpreadsheet() {
        assertEquals(application.closeSpreadSheet(), false);
        assertEquals(application.addSpreadSheet("SS1"), true);
        assertEquals(application.openSpreadSheet("SS1"), true);
        assertEquals(application.closeSpreadSheet(), true);
        assertEquals(application.closeSpreadSheet(), false);
    }

    @Test
    public void deleteSpreadsheet() {
        assertEquals(application.deleteSpreadSheet("SS1"), false);
        assertEquals(application.addSpreadSheet("SS1"), true);
        assertEquals(application.deleteSpreadSheet("SS1"), true);
        assertEquals(application.addSpreadSheet("SS1"), true);
        assertEquals(application.deleteSpreadSheet("SS1"), true);
        assertEquals(application.deleteSpreadSheet("SS1"), false);
        assertEquals(application.existsSpreadSheet("SS1"), false);
        assertEquals(application.addSpreadSheet("SS1"), true);
        assertEquals(application.openSpreadSheet("SS1"), true);
        assertEquals(application.deleteSpreadSheet("SS1"), true);
        Spreadsheet ss = application.getWorkingSpreadSheet();
        assertEquals(ss, null);
        assertEquals(application.addSpreadSheet("SS1"), true);
        assertEquals(application.openSpreadSheet("SS1"), true);
        assertEquals(application.deleteSpreadSheet("HOLA"), false);
    }

    @Test(expected = NoSpreadsheetOpen.class)
    public void getInexistentCellWhenNoSpreadsheetOpen() throws Exception {
        application.getCell(locBuilder.build("hoja1.76-d"));
    }

    @Test(expected = InvalidSheet.class)
    public void getInexistentCellFromInvalisSheet() throws Exception {
        application.addSpreadSheet("SS1");
        application.openSpreadSheet("SS1");
        application.getCell(locBuilder.build("hoja1.76-d"));
    }

    @Test(expected = CellWithoutContent.class)
    public void getEmptyCell() throws Exception {
        application.addSpreadSheet("SS1");
        application.openSpreadSheet("SS1");
        Spreadsheet ss = application.getWorkingSpreadSheet();
        ss.addSheet("hoja1");
        Cell cell = application.getCell(locBuilder.build("hoja1.76-d"));
        cell.getValue();
    }

    @Test
    public void getCellWithValue() throws Exception {
        application.addSpreadSheet("SS1");
        application.openSpreadSheet("SS1");
        Spreadsheet ss = application.getWorkingSpreadSheet();
        ss.addSheet("hoja1");
        CellLocation cellLoc = locBuilder.build("hoja1.76-d");
        ss.write(cellLoc, new FactoryContent().createContent("=76"));
        Cell cell = application.getCell(cellLoc);
        assertEquals(76, cell.getValue(), 0);
    }

    @Test
    public void processGetCellValue() throws Exception {
        application.addSpreadSheet("SS1");
        application.openSpreadSheet("SS1");
        application.getWorkingSpreadSheet().addSheet("hoja");
        application.getWorkingSpreadSheet().write(locBuilder.build("hoja.56-d"), new Number(8));
        String result = application.process("GetCellValue hoja.56-d");
        assertEquals(result, "8");
    }

    @Test
    public void processUndo() throws Exception {
        this.processGetCellValue();
        String result = application.process("Undo");
        assertEquals(result, "Error: Nothing to undo.");
    }

    @Test
    public void processWriteCell() throws Exception {
        application.addSpreadSheet("SS1");
        application.openSpreadSheet("SS1");
        application.getWorkingSpreadSheet().addSheet("hoja");
        assertEquals(application.getCell(locBuilder.build("hoja.6-f")).getValueAsString(), "No Value");
        String result = application.process("writeCell hoja.6-f =7 + 5");
        assertEquals(result, "Done");
        assertEquals(12, application.getCell(locBuilder.build("hoja.6-f")).getValue(), 0);
    }

    @Test
    public void processCorrectUndo() throws Exception {
        this.processWriteCell();
        String result = application.process("Undo");
        assertEquals(result, "Done");
        assertEquals(application.getCell(locBuilder.build("hoja.6-f")).getValueAsString(), "No Value");
    }

    @Test
    public void processRedo() throws Exception {
        this.processCorrectUndo();
        String result = application.process("Redo");
        assertEquals(result, "Done");
        assertEquals(application.getCell(locBuilder.build("hoja.6-f")).getValue(), 12, 0);
    }

    @Test
    public void processInvalidCommand() {
        String result = application.process("Esto no es un comando.");
        assertThat(result, containsString("Error: This is not a valid command."));
    }

    @Test
    public void processSetMoneyType() {
        application.process("createSpreadsheet SS1");
        application.process("openSpreadsheet SS1");
        application.process("createSheet hoja1");
        application.process("writecell hoja1.1-A 5");
        String result = application.process("setcelltype hoja1.1-A Currency");
        assertEquals("Done", result);
        result = application.process("getcellvalue hoja1.1-A");
        assertEquals("5", result);
        result = application.process("setcellformat hoja1.1-A symbol U$S");
        assertEquals("Done", result);
        result = application.process("getcellvalue hoja1.1-A");
        assertEquals("U$S 5", result);
    }

    @Test
    public void processSetDecimalFormat() {
        application.process("createSpreadsheet SS1");
        application.process("openSpreadsheet SS1");
        application.process("createSheet hoja1");
        application.process("writecell hoja1.1-A 5");
        String result = application.process("setcellformat hoja1.1-A decimal 3");
        assertEquals("Done", result);
        result = application.process("getcellvalue hoja1.1-A");
        assertEquals("5.000", result);
    }

    //@Ignore
    @Test
    public void concatTest() {
        application.process("createSpreadsheet SS1");
        application.process("openSpreadsheet SS1");
        application.process("createSheet hoja1");
        application.process("writecell hoja1.1-A hola");
        application.process("writecell hoja1.2-A 20");
        application.process("writecell hoja1.3-A 30");
        application.process("writecell hoja1.4-A reloco");

        String result = application.process("writecell hoja1.5-A = concat(hoja1.1-A,hoja1.4-A)");
        assertEquals("Done", result);
        result = application.process("getcellvalue hoja1.5-A");
        assertEquals("holareloco", result);
    }

    @Test
    public void averageTest() {
        
        application.process("createSpreadsheet SS1");
        application.process("openSpreadsheet SS1");
        application.process("createSheet hoja1");
        application.process("writecell hoja1.1-A 10");
        application.process("writecell hoja1.2-A 20");
        application.process("writecell hoja1.3-A 30");
        application.process("writecell hoja1.4-A 40");

        String result = application.process("writecell hoja1.5-A = average(hoja1.1-A:hoja1.4-A)");
        assertEquals("Done", result);
        result = application.process("getcellvalue hoja1.5-A");
        assertEquals("25", result);
    }



}
