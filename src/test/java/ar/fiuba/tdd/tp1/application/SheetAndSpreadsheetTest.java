package ar.fiuba.tdd.tp1.application;

import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import ar.fiuba.tdd.tp1.cell.content.Number;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SheetAndSpreadsheetTest {

    Spreadsheet ss = new Spreadsheet("SS1");

    @Test
    public void addSheet() {
        assertEquals(ss.addSheet("Sheet1"), true);
        assertEquals(ss.addSheet("Sheet1"), false);
        assertEquals(ss.hasSheet("Sheet1"), true);
        Sheet sheet = ss.getSheet("Sheet1");
        assertEquals(sheet.getName(), "Sheet1");
    }

    @Test(expected = InvalidSheet.class)
    public void writeSpreadsheetWithInvalidSheet() throws Exception {
        this.addSheet();
        ss.write(new CellLocationBuilder().build("Sheeeet.76-d"), new Number(7));
    }

    @Test
    public void writeSpreadsheet() throws Exception {
        this.addSheet();
        ss.write(new CellLocationBuilder().build("Sheet1.76-d"), new Number(7));
    }

}
