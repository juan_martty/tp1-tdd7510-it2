package ar.fiuba.tdd.tp1.parser;


import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import org.junit.Test;

import java.util.Arrays;

public class CreateSheetParserTest {

    CreateSheetParser parser = new CreateSheetParser();

    @Test(expected = AmountOfArgumentsInvalid.class)
    public void testParsearMenosArgumentosLevantaExcepcion() throws Exception {
        parser.parseLine(Arrays.asList("createSheet"));
    }

    @Test
    public void parsearDemasiadosArgumentosLosTomaComoDos() throws Exception {
        parser.parseLine(Arrays.asList("createSheet", "nombre de la hoja", "argumento extra"));
    }

    @Test
    public void parsearCorrectamente() throws Exception {
        parser.parseLine(Arrays.asList("createSheet", "nombre de la hoja"));
    }

}
