package ar.fiuba.tdd.tp1.parser;


import ar.fiuba.tdd.tp1.exceptions.InvalidCommand;
import org.junit.Test;

public class ParserDirectorTest {

    ParserDirector parser = new ParserDirector();

    @Test(expected = InvalidCommand.class)
    public void comandoInexistente() throws Exception {
        parser.parseLine("Esto no es un comando que exista.");
    }

    @Test(expected = InvalidCommand.class)
    public void pasarleNull() throws Exception {
        parser.parseLine(null);
    }

    @Test(expected = InvalidCommand.class)
    public void pasarleVacio() throws Exception {
        parser.parseLine("");
    }

    @Test
    public void comandoExistente() throws Exception {
        parser.parseLine("CreateSpreadSheet SS1");
    }

    /* Todos los demás casos están en los parsers individuales. */

}
