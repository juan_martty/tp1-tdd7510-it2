package ar.fiuba.tdd.tp1.parser;


import ar.fiuba.tdd.tp1.exceptions.AmountOfArgumentsInvalid;
import org.junit.Test;

import java.util.Arrays;

public class GetCellValueParserTest {

    GetCellValueParser parser = new GetCellValueParser();

    @Test(expected = AmountOfArgumentsInvalid.class)
    public void tooFewArguments() throws Exception {
        parser.parseLine(Arrays.asList("GetCellValue", "arg1", "arg2"));
    }

    @Test(expected = AmountOfArgumentsInvalid.class)
    public void tooMuchArguments() throws Exception {
        parser.parseLine(Arrays.asList("getValue"));
    }

    @Test
    public void parseCorrectly() throws Exception {
        parser.parseLine(Arrays.asList("GetCellValue", "hoja.5-aaa"));
    }

}
