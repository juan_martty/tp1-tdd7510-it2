package ar.fiuba.tdd.tp1.command;


import ar.fiuba.tdd.tp1.application.Application;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class RedoUndoCommandTest {

    Application application;
    Command redo;
    Command undo;


    @Before
    public void setUp() {
        application = new Application();
        redo = new RedoCommand();
        undo = new UndoCommand();
    }

    @Test(expected = IllegalArgumentException.class)
    public void cantExecuteRedo() throws Exception {
        redo.execute(application);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cantExecuteUndo() throws Exception {
        undo.execute(application);
    }

    @Test
    public void executeUndo() throws Exception {
        Command command = new OpenSpreadSheetCommand("SS1");
        application.addSpreadSheet("SS1");
        command.execute(application);
        application.addCommandtoChangeManager(command);
        assertNotEquals(application.getWorkingSpreadSheet(), null);
        assertEquals(application.getWorkingSpreadSheet().getName(), "SS1");
        undo.execute(application);
        assertEquals(application.getWorkingSpreadSheet(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cantExecuteAfterUndoingAlready() throws Exception {
        this.executeUndo();
        undo.execute(application);
    }

    @Test
    public void executeRedo() throws Exception {
        this.executeUndo();
        assertEquals(application.getWorkingSpreadSheet(), null);
        redo.execute(application);
        assertNotEquals(application.getWorkingSpreadSheet(), null);
        assertEquals(application.getWorkingSpreadSheet().getName(), "SS1");

    }

    @Test(expected = IllegalArgumentException.class)
    public void cantExecuteAfterRedoingAlready() throws Exception {
        this.executeRedo();
        redo.execute(application);
    }

    private void setCommands() throws Exception {

        final OpenSpreadSheetCommand openSpreadSheetCommand = new OpenSpreadSheetCommand("SS1");
        final CreateSpreadSheetCommand createSpreadSheetCommand = new CreateSpreadSheetCommand("SS1");
        final CreateSheetCommand createSheetCommand = new CreateSheetCommand("S1");
        final CloseSpreadSheetCommand closeSpreadSheetCommand = new CloseSpreadSheetCommand();

        assertEquals(application.existsSpreadSheet("SS1"), false);
        createSpreadSheetCommand.execute(application);
        application.addCommandtoChangeManager(createSpreadSheetCommand);
        assertEquals(application.existsSpreadSheet("SS1"), true);

        assertEquals(application.isASpreadSheetOpen(), false);
        openSpreadSheetCommand.execute(application);
        application.addCommandtoChangeManager(openSpreadSheetCommand);
        assertEquals(application.isASpreadSheetOpen(), true);

        assertEquals(application.getWorkingSpreadSheet().hasSheet("S1"), false);
        createSheetCommand.execute(application);
        application.addCommandtoChangeManager(createSheetCommand);
        assertEquals(application.getWorkingSpreadSheet().hasSheet("S1"), true);

        closeSpreadSheetCommand.execute(application);
        application.addCommandtoChangeManager(closeSpreadSheetCommand);
        assertEquals(application.isASpreadSheetOpen(), false);

    }

    @Test
    public void exhaustiveUndoTest() throws Exception {

        setCommands();

        undo.execute(application);
        assertEquals(application.isASpreadSheetOpen(), true);

        undo.execute(application);
        assertEquals(application.getWorkingSpreadSheet().hasSheet("S1"), false);

        undo.execute(application);
        assertEquals(application.isASpreadSheetOpen(), false);

        undo.execute(application);
        assertEquals(application.existsSpreadSheet("SS1"), false);


    }

    @Test
    public void exhaustiveRedo() throws Exception {
        this.exhaustiveUndoTest();
        redo.execute(application);
        assertEquals(application.existsSpreadSheet("SS1"), true);

        redo.execute(application);
        assertEquals(application.isASpreadSheetOpen(), true);
        assertEquals(application.getWorkingSpreadSheet().getName(), "SS1");

        redo.execute(application);
        assertEquals(application.getWorkingSpreadSheet().hasSheet("S1"), true);

        redo.execute(application);
        assertEquals(application.isASpreadSheetOpen(), false);


    }

    @Test
    public void mixRedoUndo() throws Exception {
        this.exhaustiveRedo();

        undo.execute(application);
        undo.execute(application);
        redo.execute(application);

        assertEquals(application.isASpreadSheetOpen(), true);
        assertEquals(application.existsSpreadSheet("SS1"), true);
        assertEquals(application.getWorkingSpreadSheet().getName(), "SS1");
        assertEquals(application.getWorkingSpreadSheet().hasSheet("S1"), true);
    }
}
