package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CloseSpreadSheetCommandTest {

    Application application;
    CloseSpreadSheetCommand closeSpreadSheetCommand;

    @Before
    public void setUp() {
        application = new Application();
        closeSpreadSheetCommand = new CloseSpreadSheetCommand();
    }

    @Test(expected = NoSpreadsheetOpen.class)
    public void noSpreadSheetOpen() throws Exception {
        closeSpreadSheetCommand.execute(application);
    }

    @Test
    public void spreadSheetOpen() throws Exception {
        application.addSpreadSheet("spreadSheetTest");
        application.openSpreadSheet("spreadSheetTest");
        closeSpreadSheetCommand.execute(application);
        assertEquals(application.isASpreadSheetOpen(), false);
    }

    @Test
    public void undo() throws Exception {
        this.spreadSheetOpen();
        closeSpreadSheetCommand.undo(application);
        assertEquals(application.isASpreadSheetOpen(), true);
        assertEquals(application.getWorkingSpreadSheet().getName(), "spreadSheetTest");
    }

    @Test
    public void redo() throws Exception {
        this.undo();
        closeSpreadSheetCommand.redo(application);
        assertEquals(application.isASpreadSheetOpen(), false);
    }

}