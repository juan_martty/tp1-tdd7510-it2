package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.SpreadSheetIsAlreadyOpen;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.SpreadSheetWasntCreated;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


public class OpenSpreadSheetCommandTest {

    Application application;
    OpenSpreadSheetCommand openSpreadSheetCommand;

    @Before
    public void setUp() {
        application = new Application();
        openSpreadSheetCommand = new OpenSpreadSheetCommand("Test");
    }

    @Test
    public void openSpreadsheet() throws Exception {
        application.addSpreadSheet("Test");
        openSpreadSheetCommand.execute(application);
        assertNotEquals(application.getWorkingSpreadSheet(), null);
        assertEquals(application.getWorkingSpreadSheet().getName(), "Test");
    }

    @Test(expected = SpreadSheetIsAlreadyOpen.class)
    public void spreadSheetAlreadyOpen() throws Exception {
        this.openSpreadsheet();
        openSpreadSheetCommand.execute(application);
    }

    @Test(expected = SpreadSheetWasntCreated.class)
    public void spreadSheetDoesntExist() throws Exception {
        openSpreadSheetCommand.execute(application);
    }

    @Test
    public void undo() throws Exception {
        this.openSpreadsheet();
        openSpreadSheetCommand.undo(application);
        assertEquals(application.getWorkingSpreadSheet(), null);
    }

    @Test
    public void redo() throws Exception {
        this.undo();
        openSpreadSheetCommand.redo(application);
        assertNotEquals(application.getWorkingSpreadSheet(), null);
        assertEquals(application.getWorkingSpreadSheet().getName(), "Test");
    }

}
