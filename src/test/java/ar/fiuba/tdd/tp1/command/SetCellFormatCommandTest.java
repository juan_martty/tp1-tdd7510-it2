package ar.fiuba.tdd.tp1.command;


import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetCellFormatCommandTest {

    Application app;
    SetCellFormatCommand command;
    CellLocationBuilder builder;

    @Before
    public void setUp() {
        app = new Application();
        command = null;
        builder = new CellLocationBuilder();
    }

    @Ignore
    @Test
    public void testDecimalFormatter() throws Exception {
        app.process("createspreadsheet SS1");
        app.process("openspreadsheet SS1");
        app.process("createSheet hoja1");
        app.process("writecell hoja1.1-A 67");
        command = new SetCellFormatCommand(builder.build("hoja1.1-a"), "decimal", "7");
        command.execute(app);
        assertEquals(app.process("getValue hoja1.1-a"), "67.0000000");
    }

}
