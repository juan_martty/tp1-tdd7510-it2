package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.SheetAlredyExists;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test created to prove the proper functionality of the CreateSheet-parser.
 */
public class CreateSheetCommandTest {

    Application application;
    CreateSheetCommand createSheetCommand;

    @Before
    public void setUp() {
        application = new Application();
        createSheetCommand = new CreateSheetCommand("Page1");
    }

    @Test(expected = NoSpreadsheetOpen.class)
    public void noSpreadSheetOpen() throws Exception {
        createSheetCommand.execute(application);
    }

    @Test
    public void createSheet() throws Exception {
        application.addSpreadSheet("Test");
        application.openSpreadSheet("Test");
        createSheetCommand.execute(application);
        assertEquals(application.getWorkingSpreadSheet().hasSheet("Page1"), true);
    }

    @Test(expected = SheetAlredyExists.class)
    public void sheetAlreadyExists() throws Exception {
        this.createSheet();
        createSheetCommand.execute(application);

    }

    @Test
    public void undo() throws Exception {
        this.createSheet();
        createSheetCommand.undo(application);
        assertEquals(application.getWorkingSpreadSheet().hasSheet("Page1"), false);
    }

    @Test
    public void redo() throws Exception {
        this.undo();
        createSheetCommand.redo(application);
        assertEquals(application.getWorkingSpreadSheet().hasSheet("Page1"), true);
    }

}