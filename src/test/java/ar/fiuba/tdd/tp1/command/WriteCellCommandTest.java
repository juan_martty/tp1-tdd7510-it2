package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.cell.CellLocation;
import ar.fiuba.tdd.tp1.cell.CellLocationBuilder;
import ar.fiuba.tdd.tp1.cell.FactoryContent;
import ar.fiuba.tdd.tp1.cell.content.CellContent;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.InvalidSheet;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.NoSpreadsheetOpen;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test created to prove the proper functionality of the writeCell-command.
 * It assumes the correct functionality of the cell's Location builder and
 * it assumes that the entry content is valid.
 */
public class WriteCellCommandTest {

    Application application;
    WriteCellCommand writeCellCommand;
    CellLocationBuilder builder;

    @Before
    public void setUp() {
        application = new Application();
        writeCellCommand = null;
        builder = new CellLocationBuilder();
    }

    @Test(expected = NoSpreadsheetOpen.class)
    public void noSpreadSheetOpen() throws Exception {
        CellLocation cellLocation = builder.build("S1.3-A");
        CellContent cellContent = (new FactoryContent()).createContent("5");
        writeCellCommand = new WriteCellCommand(cellLocation, cellContent);
        writeCellCommand.execute(application);
    }

    @Test(expected = InvalidSheet.class)
    public void sheetInCellLocationDoesntExist() throws Exception {
        CellLocation cellLocation = builder.build("S1.3-A");
        CellContent cellContent = (new FactoryContent()).createContent("5");
        writeCellCommand = new WriteCellCommand(cellLocation, cellContent);

        application.addSpreadSheet("SS1");
        application.openSpreadSheet("SS1");

        writeCellCommand.execute(application);
    }

    public Application getApplication() {
        return this.application;
    }

    @Test
    public void correctExecutionNumberValue() throws Exception {
        CellLocation cellLocation = builder.build("S1.3-A");
        CellContent cellContent = (new FactoryContent()).createContent("5");
        writeCellCommand = new WriteCellCommand(cellLocation, cellContent);

        application.addSpreadSheet("SS1");
        application.openSpreadSheet("SS1");

        application.getWorkingSpreadSheet().addSheet("S1");
        assertEquals(application.getWorkingSpreadSheet().hasSheet("S1"), true);


        assertEquals(application.getCell(cellLocation).getValueAsString(), "No Value");
        writeCellCommand.execute(application);
        assertEquals(application.getCell(cellLocation).getValueAsString(), "5");

    }

    @Test
    public void correctExecutionFormulaValue() throws Exception {
        CellLocation cellLocation = builder.build("S1.3-A");
        CellContent cellContent = (new FactoryContent()).createContent("= S1.1-A + S1.3-B");
        writeCellCommand = new WriteCellCommand(cellLocation, cellContent);

        application.addSpreadSheet("SS1");
        application.openSpreadSheet("SS1");
        application.getWorkingSpreadSheet().addSheet("S1");

        writeCellCommand.execute(application);
    }


    @Test
    public void undo() throws Exception {
        CellLocation cellLocation = builder.build("S1.3-A");
        this.correctExecutionNumberValue();
        writeCellCommand.undo(application);
        assertEquals(application.getCell(cellLocation).getValueAsString(), "No Value");
    }

    @Test
    public void redo() throws Exception {
        CellLocation cellLocation = builder.build("S1.3-A");
        this.correctExecutionNumberValue();
        writeCellCommand.redo(application);
        assertEquals(application.getCell(cellLocation).getValueAsString(), "5");

    }


}