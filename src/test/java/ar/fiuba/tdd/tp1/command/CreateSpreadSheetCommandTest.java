package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.application.Application;
import ar.fiuba.tdd.tp1.exceptions.spreadsheet0and0sheet0management.SpreadSheetAlredyExists;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test created to prove the proper functionality of the CreateSpreadSheet-parser.
 */
public class CreateSpreadSheetCommandTest {

    Application application;
    CreateSpreadSheetCommand createSpreadSheetCommand;

    @Before
    public void setUp() {
        application = new Application();
        createSpreadSheetCommand = new CreateSpreadSheetCommand("Page1");
    }

    @Test(expected = SpreadSheetAlredyExists.class)
    public void spreadSheetAlreadyExists() throws Exception {
        application.addSpreadSheet("Page1");
        createSpreadSheetCommand.execute(application);
    }

    @Test
    public void createSpreadSheet() throws Exception {
        createSpreadSheetCommand.execute(application);
        assertEquals(application.existsSpreadSheet("Page1"), true);
    }

    @Test
    public void undo() throws Exception {
        this.createSpreadSheet();
        createSpreadSheetCommand.undo(application);
        assertEquals(application.existsSpreadSheet("Page1"), false);
    }

    @Test
    public void redo() throws Exception {
        this.undo();
        createSpreadSheetCommand.redo(application);
        assertEquals(application.existsSpreadSheet("Page1"), true);
    }

}